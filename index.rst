.. ChemTest documentation master file, created by
   sphinx-quickstart on Thu Mar 11 14:29:00 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ChemTest's documentation!
====================================

Chem-test is a python package for benchmarking chemistry related deep models. The package provide a framework to helping scientists and developpers implement benchmark of deep-learning model with a rigourous methodologie. The framework provide:

* Training loop automation (with checkpoint management)
* Data preprocessing automation
* Metrics calculation
* Hyperparameters management (and optimization W.I.P.)
* Unit test automation
* Tensorboard automation
* Compatible with pytorch and tensorflow 2.0 (only with pytorch for now, work in progress)
* Command line tools to help development and usage of deep-learning models

.. toctree::
   :glob:
   :maxdepth: 1
   :caption: Getting started:

   getting_started/installation
   getting_started/quick_start
   getting_started/examples
   getting_started/command_line

.. toctree::
   :glob:
   :maxdepth: 1
   :caption: API reference:
   
   api_reference/data
   api_reference/datasets
   api_reference/hparams
   api_reference/metrics
   api_reference/models
   api_reference/training


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
