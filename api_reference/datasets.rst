Datasets
========

Core
----

.. autoclass:: chemtest.core.data.Dataset
    :members:
    :undoc-members:

.. autoclass:: chemtest.core.data.Set
    :members:
    :undoc-members:

Datasets
--------

.. automodule:: chemtest.datasets
    :members:
    :undoc-members:
    :show-inheritance:

