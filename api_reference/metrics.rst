Metrics
=======

Core
----

.. autoclass:: chemtest.core.metrics.Metric
    :members:
    :undoc-members:

.. autoclass:: chemtest.core.metrics.Loss
    :members: name
    :show-inheritance:

.. autoclass:: chemtest.core.metrics.Metrics
    :members:
    :undoc-members:

Metrics
-------

.. autoclass:: chemtest.metrics.RegressionMetric
    :members:
    :undoc-members:
    :show-inheritance:

.. autoclass:: chemtest.metrics.MSEMetric
    :members:
    :show-inheritance:

.. autoclass:: chemtest.metrics.MAEMetric
    :members:
    :show-inheritance:

