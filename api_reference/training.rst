Training
========

Core
----

.. autoclass:: chemtest.core.Training
    :members:
    :undoc-members:

.. autoclass:: chemtest.core.Schelude
    :members:
    :undoc-members:

.. autoclass:: chemtest.core.TrainingConfig
    :members:
    :undoc-members:

.. autoclass:: chemtest.core.EventSchelude
    :members:
    :undoc-members:

Training
--------

.. automodule:: chemtest.training
    :members:
    :undoc-members:
    :show-inheritance:

