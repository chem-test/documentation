Data convertion & dataloader
============================

Core
----

.. autoclass:: chemtest.core.data.Framework
    :members:
    :undoc-members:

.. autoclass:: chemtest.core.data.Padding
    :members:
    :undoc-members:

Data loader
^^^^^^^^^^^

.. autoclass:: chemtest.core.data.DataLoader
    :members:
    :undoc-members:

.. autoclass:: chemtest.core.data.Batch
    :members:
    :undoc-members:

Preprocessing
^^^^^^^^^^^^^

.. autoclass:: chemtest.core.data.Dependency
    :members:
    :undoc-members:

.. autoclass:: chemtest.core.data.Preprocessing
    :members:
    :undoc-members:

.. autoclass:: chemtest.core.data.DataType
    :members: gather, to_framework, reindex, get_name

.. note::

    In case of data indexation, the concatenation of multiple sample can
    break the indexed data. But the data can be reindexed with the *reindex*
    function to keep matching here target. An example with edge like data.

    .. code-block:: python

        # define vertices
        a = DataType(np.random.randn(9))
        b = DataType(np.random.randn(4))
        c = DataType(np.random.randn(6))

        # define edges by indexing the vertices
        e_a = DataType(np.array([[0, 1], [2, 3], [4, 5], [6, 7], [8, 0]]), indexing=DataType)
        e_b = DataType(np.array([[0, 1], [0, 2], [0, 3]]), indexing=DataType)
        e_c = DataType(np.array([[0, 1], [2, 3], [4, 5], [4, 5]]), indexing=DataType)

        # gather data into batch
        out = DataType.gather([a, b, c], Padding.CONCATENATE)
        e_out = DataType.gather([e_a, e_b, e_c], Padding.CONCATENATE)

        # reindex the edge to match the concatenated vertices
        e_out.reindex(out)

        print(e_out.data)
        print(e_out.sample_index)
        print(e_out.sample_size)

    Output:

    .. code-block:: python

        [[ 0  1]
         [ 2  3]
         [ 4  5]
         [ 6  7]
         [ 8  0]
         [ 9 10]
         [ 9 11]
         [ 9 12]
         [13 14]
         [15 16]
         [17 18]
         [17 18]]
        [0 0 0 0 0 1 1 1 2 2 2 2]
        [5 3 4]

To manage automatic normalization, the class variable **normalize** can be
set with an initialised :class:`chemtest.core.data.Normalize` object.

.. code-block:: python

    class CustomType(DataType):
        normalize = Normal(mean = 1.0, std = 2.0)

    CustomType.normalize = Normal(mean = -1.0, std = 2.0)

.. warning::

    Normalization is done at the object creation and is not updated in the
    **gather** function to prevent redondant operations. Change the
    **normalize** class variable after the DataType initialization has no
    effect.

.. autoclass:: chemtest.core.data.Normalize
    :members:
    :undoc-members:

.. autoclass:: chemtest.core.data.Normal
    :members:
    :undoc-members:

.. autoclass:: chemtest.core.data.DataConversion
    :members:
    :undoc-members:

Dependency tree
^^^^^^^^^^^^^^^

.. autoclass:: chemtest.core.data.DependencyTree
    :members:
    :undoc-members:

.. autoclass:: chemtest.core.data.DependencyTypeNode
    :members:
    :undoc-members:

.. autoclass:: chemtest.core.data.DependencyConvNode
    :members:
    :undoc-members:

Data types
----------

.. automodule:: chemtest.datasets.types
    :members:
    :undoc-members:

Data convertion
---------------

.. automodule:: chemtest.datasets.convertions
    :members:
    :undoc-members:
