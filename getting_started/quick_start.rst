Quick start
===========

Chem-test is building around two main abstract objects. To train a deep learning model, the model must be defined by implementing a :class:`chemtest.core.Module` and the training process must be defined into a :class:`chemtest.core.Training`. This tutorial will show step by step how to train the SchNet model (from `schnetpack <https://github.com/atomistic-machine-learning/schnetpack>`_) with minimalist example. A more in depth example is provide in the :doc:`/getting_started/examples` section.

Subject covered in this tutorial:

* Declaration of a deep-learning model
* Registration of Hyperparameters
* Declaration of a training class
* Setup metrics
* Basic usage of the command line tools provide by chem-test

Implementation
--------------

First, let's import required modules and classes. The classes imported from chem-test won't be detail here to keep this tutorial simple. Other imported modules are used to define the deep model, the optimizer and the loss function.

.. code-block:: python

    from chemtest.core import Module, Training
    from chemtest.core.hparam import HParams
    from chemtest.core.data import Framework, Padding, Set
    from chemtest.core.metrics import Loss
    from chemtest.datasets.types import *
    from chemtest.metrics import *

    # import schnetpack to get SchNet
    import schnetpack
    from schnetpack import Properties

    # import Adam to use it as an optimizer
    from torch.optim import Adam
    # import functional to define the loss function
    import torch.nn.functional as F


Model
^^^^^

To start the implementation of the model, we start by defining a class derived from :class:`chemtest.core.Module` and call it **SchNet**. As we see, hyperparameters of this class are defined into a dictionnary named *_hparams_default* (wich is a class variable i.e. a variable common to all the class instance.). To register the default hyperparameters, the decorator :meth:`chemtest.core.hparam.HParams.register_default()` is used. As shown in this example, the hyperparameters can be nested dictionary.

.. code-block:: python

    @HParams.register_default
    class SchNet(Module):
        _hparams_default = {
            "schnet": {"n_atom_basis": 64, "n_filters": 128, "n_interactions": 6},
            "atomwise": {"n_layers": 2},
        }

Then, the framework class variable is used to setup the **SchNet** module as a pytorch module. Input and output types must be defined. This step is important because chem-test will automatically preprocess the input dataset to provide the corresponding data type. In this example, the input data are the positions of the molecule as a point cloud, the corresponding atomic numbers of each atom, the matrix and offset of the cell and a the neighbours of each atom (the neighbours are the edge of the graph representation of the molecule.). Each input can be padded if the size of the data are not constant. In this case, the count, an atom of a molecule is not constant from a molecule to another. To stack all the input sample into a batch representation, chem-test can automatically pad the data and generate a mask. If a mask is required, the input data will be a tuple containing the data and the mask instead of a simple tensor representation.

.. code-block:: python

    framework = Framework.PYTORCH
    inputs_type = [
        (PointCloud, Padding.PADDING_WITHOUT_MASK),
        (AtomicNumber, Padding.PADDING_WITH_MASK),
        (MatrixCell, Padding.PADDING_WITHOUT_MASK),
        (CellOffset, Padding.PADDING_WITHOUT_MASK),
        (SchnetNeighbors, Padding.PADDING_WITHOUT_MASK),
    ]
    outputs_type = [(QM9Normalized, Padding.PADDING_WITHOUT_MASK)]

Two methods should be implemented to be considered as a valid Module. The goal of the **build** function is to setup the deep-learning model and the **forward** function define the forward propagation through the network. Inside of the **build** function, the SchNet model is initialised. This method takes an :class:`chemtest.core.hparam.HParamsNode` as input. The model should be built according to the given hyperparameters. In the current **build** function, we can see how the hyperparameters are accessed. Previously, we setup the *_hparams_default* class variable as a dictionary containing four parameters (i.e. `schnet/n_atom_basis`, `schnet/n_filters`, `schnet/n_interactions` and `schnet/n_layers`). The properties can be acquired by using the accessors of :class:`chemtest.core.hparam.HParamsNode` *hparam*. So the `schnet/n_atom_basis` parameters can be accessed with *hparams.schnet.n_atom_basis*. To finish the building process, the property *self.model* should be set. This property will be used for automatic saving and loading of the model.

.. code-block:: python

    def build(self, hparams):
        # create model
        reps = schnetpack.representation.SchNet(
            n_atom_basis=hparams.schnet.n_atom_basis,
            n_filters=hparams.schnet.n_filters,
            n_interactions=hparams.schnet.n_interactions,
        )
        output = schnetpack.atomistic.Atomwise(
            n_in=hparams.schnet.n_atom_basis,
            n_out=len(QM9Properties.properties),
            n_layers=hparams.atomwise.n_layers,
        )
        self.model = schnetpack.atomistic.AtomisticModel(reps, output)

Finally, we can implement the **forward** function to finish the model's implementation. To use the SchNet model from `schnetpack <https://github.com/atomistic-machine-learning/schnetpack>`_, we sould provide a dictionnary with moleculare properties. This is done with the variable *schnet_inputs*. After doing the forward propagation, the output of the model is set inside of a list accordingly to the *outputs_type* configuration.

.. code-block:: python

    def forward(self, inputs):
        schnet_inputs = {
            Properties.R: inputs[0],
            Properties.Z: inputs[1][0],
            Properties.atom_mask: inputs[1][1],
            Properties.cell: inputs[2],
            Properties.cell_offset: inputs[3],
            Properties.neighbors: inputs[4],
            Properties.neighbor_mask: None,
        }
        out = self.model(schnet_inputs)
        properties = out["y"]
        return [properties]

Training
^^^^^^^^

After setting up the SchNet :class:`chemtest.core.Module`, the model need a class derived from :class:`chemtest.core.Training` for the training process. The training loop is automated and only few elements must be set to be valid. The model, the optimizer and the loss function should be defined.

We start by creating a **TrainSchNet** class and register de default hyperparameters.

.. code-block:: python

    @HParams.register_default
    class TrainSchNet(Training):
        _hparams_default = {
            "optimizer": {"lr": 1e-4},
            "batch_size": 32,
        }

Then, a **init** function must be defined. The goal of the **init** function is to setup the model and optimizer, but also to setup the metrics. To register metrics, the method :meth:`chemtest.core.metrics.Metrics.add_metric()` from the class attribute *self.metrics* can be used. **add_metric** take a :class:`chemtest.core.metrics.Metric` class as first argument and a list of :class:`chemtest.core.data.Set`. These two arguments represente the targeted metric and the sets on wich it will be calculated.

.. code-block:: python

    def init(self):
        self.model = SchNet(self.global_hparams)

        self.optimizer = Adam(
            self.model.model.parameters(), lr=self.hparams.optimizer.lr
        )

        self.metrics.add_metric(Loss(), on_sets=[Set.TRAINING, Set.VALIDATION])

        self.metrics.add_metric(
            MAEMetric(
                input_types=[(QM9Normalization, Padding.NONE)],
                prediction_types=[QM9Normalized],
            ),
            on_sets=[Set.VALIDATION, Set.TESTING],
        )

Then, a **init** function must be defined. The goal of the **init** function is to setup the model and optimizer, but also to setup the metrics. To register metrics, the method :meth:`chemtest.core.metrics.Metrics.add_metric()` from the class attribute *self.metrics* can be used. **add_metric** take a :class:`chemtest.core.metrics.Metric` class as first argument and a list of :class:`chemtest.core.data.Set`. These two arguments represente the targeted metric and the sets on wich it will be calculated.

.. code-block:: python

    def loss_batch(self, outputs, targets):
        return F.mse_loss(outputs[0], targets[0], reduction="mean")

Command line
------------

Now, the implementation is finished. To use the implemented model, chem-test provides command line tools to train the model.

Generate Hyperparameters
^^^^^^^^^^^^^^^^^^^^^^^^

To generate a file containing all the default parameters, the following command can be used.

.. code-block:: bash

    chem-test default ./getting_started.py -o my_hyperparameters.json

The `my_hyperparameters.json` file should contains the following data.

.. code-block:: json

    {
        "module": {
            "schnet": {
                "atomwise": {
                    "n_layers": 2
                },
                "schnet": {
                    "n_atom_basis": 64,
                    "n_filters": 128,
                    "n_interactions": 6
                }
            }
        },
        "training": {
            "trainschnet": {
                "batch_size": 32,
                "optimizer": {
                    "lr": 0.0001
                }
            }
        }
    }

Start training
^^^^^^^^^^^^^^

To start the training process, the `chem-test train` command can be used. The first argument is the targeted training class (in this case TrainSchNet) as path/to/file.class_name. The second argument is the dataset's name. Custom hyperparameters can be specified with the *--hparams* argument and the training, validation and testing set can be configured with the *--dataset-split* argument (0.8 0.1 0.1 mean 80% of the dataset is used for training, 10% for validation and 10% for testing.). The training process will run for one epoch by default.

.. code-block:: bash

    chem-test train ./getting_started.TrainSchNet QM9 --hparams my_hyperparameters.json --dataset-split 0.8 0.1 0.1

The training of the model can last long before starting because of the downloading of the QM9 dataset. The complete documentation of the command line tools can be found in :doc:`/getting_started/command_line`.

Generated files
---------------

As a result, chem-test will generate output files and directories. To give a rapid overview, the `data` directory contains the datasets (e.g. QM9), the `checkpoints` directory contains all the generated checkpoints and the `runs` directory contains tensorboard logs and can be viewed with:

.. code-block:: bash

    tensorboard --logdir runs

Metrics are saved inside of the checkpoints and as tensorboard logs and the impact of the hyperparameters can be viewed in the **HPARAMS** section. All the directories can be configured manually.

Conclusion
----------

This tutorial provides a simple but working example of how to use chem-test. To summarize, benchmarking models with chem-test require:

* A model derived from :class:`chemtest.core.Module` and implementing **build** and **forward**
* A training class derived from :class:`chemtest.core.Training` and implementing **init** and **loss_batch**
* the `chem-test train` command to start the training process

For more in-depth explanation, please follow the advanced example in the :doc:`/getting_started/examples` section and feel free to read the API reference.
