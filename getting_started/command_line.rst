Command line tools
==================

Check version
-------------

.. code-block:: bash

    chem-test -V

Check unit test
---------------

.. code-block:: bash

    chem-test test

Generate default hyperparameters
--------------------------------

.. code-block:: bash

    chem-test default module1.class1 module2.class2 -o hyperparameters_file.json

Train a model
-------------

.. code-block:: bash

    chem-test train

