Installation
============

Requierment
-----------

Chem-test support python 3.7/3.8 and require:

* torch>=1.8
* tensorflow>=2.4.1
* schnetpack>=0.3
* tqdm>=4.59

Download
--------

The source code is available on gitlab.

.. code-block:: bash

    git clone https://gitlab.com/chem-test/python-chemtest.git


System wide
-----------

Then, install chem-test with pip and the `setup.py`

.. code-block:: bash

    pip install git+https://gitlab.com/chem-test/python-chemtest.git

Virtual env
-----------

Chem-test can be installed in a virtual env

.. code-block:: bash

    virtualenv -p python3 venv
    source venv/bin/activate
    pip install git+https://gitlab.com/chem-test/python-chemtest.git

Check installed version
-----------------------

.. code-block:: bash

    chem-test -V

Unit testing
------------

Once chem-test is installed, unit testing can be performed.

.. code-block:: bash

    chem-test test
